
def vowel_swapper(string):
    # ==============
    # Your code here
    vowels = ["a", "e", "i", "o", "u", "A", "E", "I", "O", "U"]
    new_string = ""
    for letter in string:
        if letter not in vowels:
            new_string += letter
        else:
            if letter == "a" or letter == "A":
                new_string += "4"
            if letter == "e" or letter == "E":
                new_string += "3"
            if letter == "i" or letter == "I":
                new_string += "!"
            if letter == "o":
                new_string += "ooo"
            if letter == "O":
                new_string += "000"
            if letter == "u" or letter == "U":
                new_string += "|_|"
    return new_string

    # ==============
    # Should print "a4a e3e i!i o000o u|_|u" to the console
print(vowel_swapper("aAa eEe iIi oOo uUu"))
# Should print "Hello Wooorld" to the console
print(vowel_swapper("Hello World"))
# Should print "Ev3rything's Av4!lable" to the console
print(vowel_swapper("Everything's Available"))
