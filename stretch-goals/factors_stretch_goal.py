def factors(number):
    # ==============
    # Your code here
    mylist = []
    for num in range(2, number):
        if number % num == 0:
            mylist.append(num)
    if not mylist:
        return(f"{number} is a prime number")
    else:
        return mylist

    # ==============
print(factors(15))  # Should print [3, 5] to the console
print(factors(12))  # Should print [2, 3, 4, 6] to the console
print(factors(13))  # Should print “13 is a prime number”
